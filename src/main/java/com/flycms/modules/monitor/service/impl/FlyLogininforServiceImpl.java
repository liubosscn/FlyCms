package com.flycms.modules.monitor.service.impl;

import java.util.List;

import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.monitor.domain.FlyJob;
import com.flycms.modules.monitor.domain.FlyLogininfor;
import com.flycms.modules.monitor.mapper.FlyLogininforMapper;
import com.flycms.modules.monitor.service.IFlyLogininforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 系统访问日志情况信息 服务层处理
 * 
 * @author kaifei sun
 */
@Service
public class FlyLogininforServiceImpl implements IFlyLogininforService
{

    @Autowired
    private FlyLogininforMapper logininforMapper;

    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增系统登录日志
     * 
     * @param logininfor 访问日志对象
     */
    @Override
    public void insertLogininfor(FlyLogininfor logininfor)
    {
        logininfor.setInfoId(SnowFlakeUtils.nextId());
        logininforMapper.insertLogininfor(logininfor);
    }

    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除系统登录日志
     *
     * @param infoIds 需要删除的登录日志ID
     * @return
     */
    @Override
    public int deleteLogininforByIds(Long[] infoIds)
    {
        return logininforMapper.deleteLogininforByIds(infoIds);
    }

    /**
     * 清空系统登录日志
     */
    @Override
    public void cleanLogininfor()
    {
        logininforMapper.cleanLogininfor();
    }

    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询系统登录日志集合列表翻页
     *
     * @param logininfor
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return 登录记录集合
     */
    @Override
    public Pager<FlyLogininfor> selectLogininforPager(FlyLogininfor logininfor, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<FlyLogininfor> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(logininfor);
        pager.setList(logininforMapper.selectLogininforPager(pager));
        pager.setTotal(logininforMapper.queryLogininforTotal(pager));
        return pager;
    }

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    @Override
    public List<FlyLogininfor> exportLogininforList(FlyLogininfor logininfor)
    {
        return logininforMapper.exportLogininforList(logininfor);
    }
}
