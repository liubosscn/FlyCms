package com.flycms.modules.monitor.service;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.monitor.domain.FlyLogininfor;

/**
 * 系统访问日志情况信息 服务层
 * 
 * @author kaifei sun
 */
public interface IFlyLogininforService
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增系统登录日志
     *
     * @param logininfor 访问日志对象
     */
    public void insertLogininfor(FlyLogininfor logininfor);
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除系统登录日志
     *
     * @param infoIds 需要删除的登录日志ID
     * @return
     */
    public int deleteLogininforByIds(Long[] infoIds);

    /**
     * 清空系统登录日志
     */
    public void cleanLogininfor();

    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////

    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////


    /**
     * 查询系统登录日志集合列表翻页
     *
     * @param logininfor
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return 登录记录集合
     */
    public Pager<FlyLogininfor> selectLogininforPager(FlyLogininfor logininfor, Integer page, Integer limit, String sort, String order);

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    public List<FlyLogininfor> exportLogininforList(FlyLogininfor logininfor);
}
