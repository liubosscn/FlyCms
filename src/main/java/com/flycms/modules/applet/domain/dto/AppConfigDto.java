package com.flycms.modules.applet.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 小程序开发者ID设置数据传输对象 fly_app_config
 * 
 * @author admin
 * @date 2020-05-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class AppConfigDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 平台名称 */
    @Excel(name = "平台名称")
    private String platform;
    /** AppId */
    @Excel(name = "AppId")
    private String appId;
    /** 申请网址 */
    @Excel(name = "申请网址")
    private String platformUrl;
}
