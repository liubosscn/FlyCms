package com.flycms.modules.site.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.site.domain.SiteAboutClassify;
import com.flycms.modules.site.domain.dto.SiteAboutClassifyDTO;
import com.flycms.modules.site.service.ISiteAboutClassifyService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 关于我们分类Controller
 * 
 * @author admin
 * @date 2021-01-27
 */
@RestController
@RequestMapping("/system/site/about/classify")
public class SiteAboutClassifyAdminController extends BaseController
{
    @Autowired
    private ISiteAboutClassifyService siteAboutClassifyService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增关于我们分类
     */
    @PreAuthorize("@ss.hasPermi('site:classify:add')")
    @Log(title = "关于我们分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SiteAboutClassify siteAboutClassify)
    {
        return toAjax(siteAboutClassifyService.insertSiteAboutClassify(siteAboutClassify));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除关于我们分类
     */
    @PreAuthorize("@ss.hasPermi('site:classify:remove')")
    @Log(title = "关于我们分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(siteAboutClassifyService.deleteSiteAboutClassifyByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改关于我们分类
     */
    @PreAuthorize("@ss.hasPermi('site:classify:edit')")
    @Log(title = "关于我们分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SiteAboutClassify siteAboutClassify)
    {
        return toAjax(siteAboutClassifyService.updateSiteAboutClassify(siteAboutClassify));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询关于我们分类列表
     */
    @PreAuthorize("@ss.hasPermi('site:classify:list')")
    @GetMapping("/list")
    public TableDataInfo list(SiteAboutClassify siteAboutClassify,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SiteAboutClassifyDTO> pager = siteAboutClassifyService.selectSiteAboutClassifyPager(siteAboutClassify, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出关于我们分类列表
     */
    @PreAuthorize("@ss.hasPermi('site:classify:export')")
    @Log(title = "关于我们分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SiteAboutClassify siteAboutClassify)
    {
        List<SiteAboutClassifyDTO> siteAboutClassifyList = siteAboutClassifyService.exportSiteAboutClassifyList(siteAboutClassify);
        ExcelUtil<SiteAboutClassifyDTO> util = new ExcelUtil<SiteAboutClassifyDTO>(SiteAboutClassifyDTO.class);
        return util.exportExcel(siteAboutClassifyList, "classify");
    }

    /**
     * 获取关于我们分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('site:classify:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(siteAboutClassifyService.findSiteAboutClassifyById(id));
    }

}
