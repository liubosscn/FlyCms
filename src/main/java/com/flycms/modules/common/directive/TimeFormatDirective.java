package com.flycms.modules.common.directive;

import com.flycms.common.utils.DateUtils;
import com.flycms.common.web.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * 文本字符串截断
 * @author: kaifei sun
 * @date:   2020年11月17日 下午4:50:51
 */
@Component
public class TimeFormatDirective implements TemplateDirectiveModel {

	public static final String PARAM_S = "time";

	@SneakyThrows
	@SuppressWarnings("unchecked")
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
						TemplateDirectiveBody body) throws TemplateException, IOException {
		String time = DirectiveUtils.getString(PARAM_S, params);
		if (time != null) {
			Writer out = env.getOut();
			out.append(DateUtils.getDateTimeString(time));
		}
	}
}
