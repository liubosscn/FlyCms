package com.flycms.modules.group.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 小组专辑帖子关联数据传输对象 fly_group_album_topic
 * 
 * @author admin
 * @date 2020-12-16
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupAlbumTopicDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 专辑ID */
    @Excel(name = "专辑ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long albumId;
    /** 帖子ID */
    @Excel(name = "帖子ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long topicId;
    /** 章节名称 */
    @Excel(name = "章节名称")
    private String chapter;
    /** 内容排序 */
    @Excel(name = "内容排序")
    private Integer sortOrder;

}
