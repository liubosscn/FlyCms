package com.flycms.modules.group.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupTopicFollow;
import com.flycms.modules.group.domain.dto.GroupTopicFollowDTO;
import com.flycms.modules.group.service.IGroupTopicFollowService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 话题关注Controller
 * 
 * @author admin
 * @date 2021-02-01
 */
@RestController
@RequestMapping("/system/group/follow")
public class GroupTopicFollowAdminController extends BaseController
{
    @Autowired
    private IGroupTopicFollowService groupTopicFollowService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增话题关注
     */
    @PreAuthorize("@ss.hasPermi('group:follow:add')")
    @Log(title = "话题关注", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GroupTopicFollow groupTopicFollow)
    {
        return toAjax(groupTopicFollowService.insertGroupTopicFollow(groupTopicFollow));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除话题关注
     */
    @PreAuthorize("@ss.hasPermi('group:follow:remove')")
    @Log(title = "话题关注", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(groupTopicFollowService.deleteGroupTopicFollowByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题关注
     */
    @PreAuthorize("@ss.hasPermi('group:follow:edit')")
    @Log(title = "话题关注", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupTopicFollow groupTopicFollow)
    {
        return toAjax(groupTopicFollowService.updateGroupTopicFollow(groupTopicFollow));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询话题关注列表
     */
    @PreAuthorize("@ss.hasPermi('group:follow:list')")
    @GetMapping("/list")
    public TableDataInfo list(GroupTopicFollow groupTopicFollow,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupTopicFollowDTO> pager = groupTopicFollowService.selectGroupTopicFollowPager(groupTopicFollow, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出话题关注列表
     */
    @PreAuthorize("@ss.hasPermi('group:follow:export')")
    @Log(title = "话题关注", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GroupTopicFollow groupTopicFollow)
    {
        List<GroupTopicFollowDTO> groupTopicFollowList = groupTopicFollowService.exportGroupTopicFollowList(groupTopicFollow);
        ExcelUtil<GroupTopicFollowDTO> util = new ExcelUtil<GroupTopicFollowDTO>(GroupTopicFollowDTO.class);
        return util.exportExcel(groupTopicFollowList, "follow");
    }

    /**
     * 获取话题关注详细信息
     */
    @PreAuthorize("@ss.hasPermi('group:follow:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(groupTopicFollowService.findGroupTopicFollowById(id));
    }

}
