package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopicColumn;
import org.springframework.stereotype.Repository;

/**
 * 帖子分类Mapper接口
 * 
 * @author admin
 * @date 2020-11-03
 */
@Repository
public interface GroupTopicColumnMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增帖子分类
     *
     * @param groupTopicColumn 帖子分类
     * @return 结果
     */
    public int insertGroupTopicColumn(GroupTopicColumn groupTopicColumn);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除帖子分类
     *
     * @param id 帖子分类ID
     * @return 结果
     */
    public int deleteGroupTopicColumnById(Long id);

    /**
     * 批量删除帖子分类
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupTopicColumnByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改帖子分类
     *
     * @param groupTopicColumn 帖子分类
     * @return 结果
     */
    public int updateGroupTopicColumn(GroupTopicColumn groupTopicColumn);


    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询帖子分类数量
     *
     * @param groupTopicColumn 帖子分类
     * @return 帖子分类数量
     */
    public int checkGroupTopicColumnUnique(GroupTopicColumn groupTopicColumn);

    /**
     * 按shortUrl查询群组(小组)分类信息
     *
     * @param shortUrl 短域名地址
     *
     * @return 群组(小组)分类
     */
    public GroupTopicColumn findGroupTopicColumnByShorturl(String shortUrl);

    /**
     * 查询帖子分类
     * 
     * @param id 帖子分类ID
     * @return 帖子分类
     */
    public GroupTopicColumn findGroupTopicColumnById(Long id);

    /**
     * 查询帖子分类数量
     *
     * @param pager 分页处理类
     * @return 帖子分类数量
     */
    public int queryGroupTopicColumnTotal(Pager pager);

    /**
     * 查询帖子分类列表
     * 
     * @param pager 分页处理类
     * @return 帖子分类集合
     */
    public List<GroupTopicColumn> selectGroupTopicColumnPager(Pager pager);

    /**
     * 查询需要导出的帖子分类列表
     *
     * @param groupTopicColumn 帖子分类
     * @return 帖子分类集合
     */
    public List<GroupTopicColumn> selectGroupTopicColumnList(GroupTopicColumn groupTopicColumn);
}
