package com.flycms.modules.group.service.impl;


import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.mapper.GroupUserIsauditMapper;
import com.flycms.modules.group.domain.GroupUserIsaudit;
import com.flycms.modules.group.domain.dto.GroupUserIsauditDTO;
import com.flycms.modules.group.service.IGroupUserIsauditService;

import java.util.ArrayList;
import java.util.List;

/**
 * 小组成员审核Service业务层处理
 * 
 * @author admin
 * @date 2020-11-24
 */
@Service
public class GroupUserIsauditServiceImpl implements IGroupUserIsauditService 
{
    @Autowired
    private GroupUserIsauditMapper groupUserIsauditMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组成员审核
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 结果
     */
    @Override
    public int insertGroupUserIsaudit(GroupUserIsaudit groupUserIsaudit)
    {
        groupUserIsaudit.setId(SnowFlakeUtils.nextId());
        return groupUserIsauditMapper.insertGroupUserIsaudit(groupUserIsaudit);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组成员审核
     *
     * @param userIds 需要删除的小组成员审核ID
     * @return 结果
     */
    @Override
    public int deleteGroupUserIsauditByIds(Long[] userIds)
    {
        return groupUserIsauditMapper.deleteGroupUserIsauditByIds(userIds);
    }

    /**
     * 删除小组成员审核信息
     *
     * @param userId 小组成员审核ID
     * @return 结果
     */
    @Override
    public int deleteGroupUserIsauditById(Long userId)
    {
        return groupUserIsauditMapper.deleteGroupUserIsauditById(userId);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组成员审核
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 结果
     */
    @Override
    public int updateGroupUserIsaudit(GroupUserIsaudit groupUserIsaudit)
    {
        return groupUserIsauditMapper.updateGroupUserIsaudit(groupUserIsaudit);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询小组成员审核
     *
     * @param userId 用户ID
     * @param groupId 群组ID
     * @return 小组成员审核
     */
    @Override
    public GroupUserIsaudit findGroupUserIsauditById(Long userId,Long groupId)
    {
        return groupUserIsauditMapper.findGroupUserIsauditById(userId,groupId);
    }


    /**
     * 查询小组成员审核列表
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 小组成员审核
     */
    @Override
    public Pager<GroupUserIsauditDTO> selectGroupUserIsauditPager(GroupUserIsaudit groupUserIsaudit, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<GroupUserIsauditDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupUserIsaudit);

        List<GroupUserIsaudit> groupUserIsauditList=groupUserIsauditMapper.selectGroupUserIsauditPager(pager);
        List<GroupUserIsauditDTO> dtolsit = new ArrayList<GroupUserIsauditDTO>();
        groupUserIsauditList.forEach(entity -> {
            GroupUserIsauditDTO dto = new GroupUserIsauditDTO();
            dto.setUserId(entity.getUserId());
            dto.setGroupId(entity.getGroupId());
            dto.setStatus(entity.getStatus());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupUserIsauditMapper.queryGroupUserIsauditTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的小组成员审核列表
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 小组成员审核集合
     */
    @Override
    public List<GroupUserIsauditDTO> exportGroupUserIsauditList(GroupUserIsaudit groupUserIsaudit) {
        return BeanConvertor.copyList(groupUserIsauditMapper.exportGroupUserIsauditList(groupUserIsaudit),GroupUserIsauditDTO.class);
    }
}
