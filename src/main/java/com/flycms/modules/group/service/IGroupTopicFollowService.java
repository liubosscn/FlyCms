package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopicFollow;
import com.flycms.modules.group.domain.dto.GroupTopicFollowDTO;

import java.util.List;

/**
 * 话题关注Service接口
 * 
 * @author admin
 * @date 2021-02-01
 */
public interface IGroupTopicFollowService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题关注
     *
     * @param groupTopicFollow 话题关注
     * @return 结果
     */
    public int insertGroupTopicFollow(GroupTopicFollow groupTopicFollow);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除话题关注
     *
     * @param ids 需要删除的话题关注ID
     * @return 结果
     */
    public int deleteGroupTopicFollowByIds(Long[] ids);

    /**
     * 删除话题关注信息
     *
     * @param id 话题关注ID
     * @return 结果
     */
    public int deleteGroupTopicFollowById(Long id);

    /**
     * 按用户id和话题id查询是否关注
     *
     * @param topicId 话题id
     * @param userId 用户id
     * @return
     */
    public int deleteTopicUserFollow(Long topicId,Long userId);
    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题关注
     *
     * @param groupTopicFollow 话题关注
     * @return 结果
     */
    public int updateGroupTopicFollow(GroupTopicFollow groupTopicFollow);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验话题关注是否唯一
     *
     * @param topicId 话题id
     * @param userId 用户id
     * @return
     */
    public String checkTopicFollowUnique(Long topicId,Long userId);

    /**
     * 查询话题关注
     * 
     * @param id 话题关注ID
     * @return 话题关注
     */
    public GroupTopicFollowDTO findGroupTopicFollowById(Long id);

    /**
     * 查询话题关注列表
     * 
     * @param groupTopicFollow 话题关注
     * @return 话题关注集合
     */
    public Pager<GroupTopicFollowDTO> selectGroupTopicFollowPager(GroupTopicFollow groupTopicFollow, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的话题关注列表
     *
     * @param groupTopicFollow 话题关注
     * @return 话题关注集合
     */
    public List<GroupTopicFollowDTO> exportGroupTopicFollowList(GroupTopicFollow groupTopicFollow);
}
