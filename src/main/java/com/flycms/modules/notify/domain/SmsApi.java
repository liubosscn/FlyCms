package com.flycms.modules.notify.domain;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 短信接口对象 fly_sms_api
 * 
 * @author admin
 * @date 2020-05-27
 */
@Data
public class SmsApi extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 接口名称 */
    private String apiName;
    /** KeyId */
    private String accessKeyId;
    /** KeySecret */
    private String accessKeySecret;
    /** 接口网址 */
    private String apiUrl;
    /** 状态 */
    private Integer status;
}
