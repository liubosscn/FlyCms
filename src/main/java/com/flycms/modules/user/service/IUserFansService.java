package com.flycms.modules.user.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.UserFans;
import com.flycms.modules.user.domain.dto.UserFansDTO;

import java.util.List;

/**
 * 用户分析查询关联表Service接口
 * 
 * @author admin
 * @date 2020-11-27
 */
public interface IUserFansService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户分析查询关联表
     *
     * @param userFans 用户分析查询关联表
     * @return 结果
     */
    public int insertUserFans(UserFans userFans);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户分析查询关联表信息
     *
     * @param followId 被关注者id
     * @param fansId 粉丝ID
     * @return 结果
     */
    public int deleteUserFansById(Long followId,Long fansId);

    /**
     * 批量删除用户分析查询关联表
     *
     * @param ids 需要删除的用户分析查询关联表ID
     * @return 结果
     */
    public int deleteUserFansByIds(Long[] ids);
    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户分析查询关联表
     *
     * @param userFans 用户分析查询关联表
     * @return 结果
     */
    public int updateUserFans(UserFans userFans);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询用户分析查询关联表
     *
     * @param followId 被关注者id
     * @param fansId 粉丝ID
     * @return 用户分析查询关联表
     */
    public Boolean checkUserFansUnique(Long followId,Long fansId);

    /**
     * 查询用户分析查询关联表
     * 
     * @param id 用户分析查询关联表ID
     * @return 用户分析查询关联表
     */
    public UserFansDTO findUserFansById(Long id);

    /**
     * 查询用户分析查询关联表列表
     * 
     * @param userFans 用户分析查询关联表
     * @return 用户分析查询关联表集合
     */
    public Pager<UserFansDTO> selectUserFansPager(UserFans userFans, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的用户分析查询关联表列表
     *
     * @param userFans 用户分析查询关联表
     * @return 用户分析查询关联表集合
     */
    public List<UserFansDTO> exportUserFansList(UserFans userFans);
}
