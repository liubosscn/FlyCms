package com.flycms.modules.user.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

/**
 * 用户和app关联数据传输对象 fly_user_app_merge
 * 
 * @author admin
 * @date 2020-05-31
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserAppMergeDto
{
    private static final long serialVersionUID = 1L;

    /** APP的id */
    @Excel(name = "APP的id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long appId;
    /** 用户id */
    @Excel(name = "用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
}
