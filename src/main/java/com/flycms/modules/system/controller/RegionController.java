package com.flycms.modules.system.controller;

import java.util.List;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.system.domain.Region;
import com.flycms.modules.system.domain.dto.RegionDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.modules.system.service.IRegionService;

/**
 * 行政区域Controller
 * 
 * @author admin
 * @date 2020-05-31
 */
@RestController
@RequestMapping("/system/region")
public class RegionController extends BaseController
{
    @Autowired
    private IRegionService regionService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增行政区域
     */
    @PreAuthorize("@ss.hasPermi('system:region:add')")
    @Log(title = "行政区域", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Region region)
    {
        if (UserConstants.NOT_UNIQUE.equals(regionService.checkRegionRegionNameUnique(region)))
        {
            return AjaxResult.error("新增行政区域'" + region.getRegionName() + "'失败，行政区域名称已存在");
        }
        return toAjax(regionService.insertRegion(region));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除行政区域
     */
    @PreAuthorize("@ss.hasPermi('system:region:remove')")
    @Log(title = "行政区域", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(regionService.deleteRegionByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改行政区域
     */
    @PreAuthorize("@ss.hasPermi('system:region:edit')")
    @Log(title = "行政区域", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Region region)
    {
        return toAjax(regionService.updateRegion(region));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询行政区域列表
     */
    @PreAuthorize("@ss.hasPermi('system:region:list')")
    @GetMapping("/list")
    public AjaxResult list(Region region)
    {
        List<RegionDTO> list = regionService.selectRegionList(region);
        return AjaxResult.success(list);
    }

    /**
     * 导出行政区域列表
     */
    @PreAuthorize("@ss.hasPermi('system:region:export')")
    @Log(title = "行政区域", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Region region)
    {
    List<RegionDTO> list = regionService.selectRegionList(region);
    ExcelUtil<RegionDTO> util = new ExcelUtil<RegionDTO>(RegionDTO.class);
    return util.exportExcel(list, "region");
    }

    /**
     * 获取行政区域详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:region:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(regionService.selectRegionById(id));
    }
}
