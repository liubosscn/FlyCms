package com.flycms.modules.system.mapper;

import java.util.List;
import com.flycms.modules.system.domain.FlyAdminPost;
import org.springframework.stereotype.Repository;

/**
 * 用户与岗位关联表 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyAdminPostMapper
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 批量新增用户岗位信息
     *
     * @param adminPostList 用户角色列表
     * @return 结果
     */
    public int batchAdminPost(List<FlyAdminPost> adminPostList);
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 通过用户ID删除用户和岗位关联
     *
     * @param adminId 用户ID
     * @return 结果
     */
    public int deleteAdminPostByAdminId(Long adminId);

    /**
     * 批量删除用户和岗位关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAdminPost(Long[] ids);
    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////

    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    public int countAdminPostById(Long postId);

}
