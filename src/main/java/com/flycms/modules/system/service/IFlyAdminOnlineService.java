package com.flycms.modules.system.service;

import com.flycms.framework.security.LoginAdmin;
import com.flycms.modules.monitor.domain.FlyAdminOnline;

/**
 * 在线用户 服务层
 * 
 * @author kaifei sun
 */
public interface IFlyAdminOnlineService
{
    /**
     * 通过登录地址查询信息
     * 
     * @param ipaddr 登录地址
     * @param user 用户信息
     * @return 在线用户信息
     */
    public FlyAdminOnline selectOnlineByIpaddr(String ipaddr, LoginAdmin user);

    /**
     * 通过用户名称查询信息
     * 
     * @param adminName 用户名称
     * @param admin 用户信息
     * @return 在线用户信息
     */
    public FlyAdminOnline selectOnlineByAdminName(String adminName, LoginAdmin admin);

    /**
     * 通过登录地址/用户名称查询信息
     * 
     * @param ipaddr 登录地址
     * @param adminName 用户名称
     * @param admin 用户信息
     * @return 在线用户信息
     */
    public FlyAdminOnline selectOnlineByInfo(String ipaddr, String adminName, LoginAdmin admin);

    /**
     * 设置在线用户信息
     * 
     * @param admin 用户信息
     * @return 在线用户
     */
    public FlyAdminOnline loginAdminToAdminOnline(LoginAdmin admin);
}
