package com.flycms.modules.system.service.impl;

import com.flycms.common.utils.StrUtils;
import com.flycms.framework.security.LoginAdmin;
import com.flycms.modules.system.service.IFlyAdminOnlineService;
import org.springframework.stereotype.Service;
import com.flycms.modules.monitor.domain.FlyAdminOnline;

/**
 * 在线用户 服务层处理
 * 
 * @author kaifei sun
 */
@Service
public class FlyAdminOnlineServiceImpl implements IFlyAdminOnlineService
{
    /**
     * 通过登录地址查询信息
     * 
     * @param ipaddr 登录地址
     * @param admin 用户信息
     * @return 在线用户信息
     */
    @Override
    public FlyAdminOnline selectOnlineByIpaddr(String ipaddr, LoginAdmin admin)
    {
        if (StrUtils.equals(ipaddr, admin.getIpaddr()))
        {
            return loginAdminToAdminOnline(admin);
        }
        return null;
    }

    /**
     * 通过用户名称查询信息
     * 
     * @param adminName 用户名称
     * @param admin 用户信息
     * @return 在线用户信息
     */
    @Override
    public FlyAdminOnline selectOnlineByAdminName(String adminName, LoginAdmin admin)
    {
        if (StrUtils.equals(adminName, admin.getUsername()))
        {
            return loginAdminToAdminOnline(admin);
        }
        return null;
    }

    /**
     * 通过登录地址/用户名称查询信息
     * 
     * @param ipaddr 登录地址
     * @param adminName 用户名称
     * @param admin 用户信息
     * @return 在线用户信息
     */
    @Override
    public FlyAdminOnline selectOnlineByInfo(String ipaddr, String adminName, LoginAdmin admin)
    {
        if (StrUtils.equals(ipaddr, admin.getIpaddr()) && StrUtils.equals(adminName, admin.getUsername()))
        {
            return loginAdminToAdminOnline(admin);
        }
        return null;
    }

    /**
     * 设置在线用户信息
     * 
     * @param admin 用户信息
     * @return 在线用户
     */
    public FlyAdminOnline loginAdminToAdminOnline(LoginAdmin admin)
    {
        if (StrUtils.isNull(admin) && StrUtils.isNull(admin.getAdmin()))
        {
            return null;
        }
        FlyAdminOnline flyAdminOnline = new FlyAdminOnline();
        flyAdminOnline.setTokenId(admin.getToken());
        flyAdminOnline.setUserName(admin.getUsername());
        flyAdminOnline.setIpaddr(admin.getIpaddr());
        flyAdminOnline.setLoginLocation(admin.getLoginLocation());
        flyAdminOnline.setBrowser(admin.getBrowser());
        flyAdminOnline.setOs(admin.getOs());
        flyAdminOnline.setLoginTime(admin.getLoginTime());
        if (StrUtils.isNotNull(admin.getAdmin().getDept()))
        {
            flyAdminOnline.setDeptName(admin.getAdmin().getDept().getDeptName());
        }
        return flyAdminOnline;
    }
}
