package com.flycms.modules.system.service.impl;

import java.util.List;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.StrUtils;
import com.flycms.modules.system.domain.FlyConfig;
import com.flycms.modules.system.mapper.FlyConfigMapper;
import com.flycms.modules.system.service.IFlyConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 参数配置 服务层实现
 * 
 * @author kaifei sun
 */
@Service
public class FlyConfigServiceImpl implements IFlyConfigService
{
    @Autowired
    private FlyConfigMapper configMapper;

    /**
     * 查询参数配置信息
     * 
     * @param configId 参数配置ID
     * @return 参数配置信息
     */
    @Override
    public FlyConfig selectConfigById(Long configId)
    {
        FlyConfig config = new FlyConfig();
        config.setConfigId(configId);
        return configMapper.selectConfig(config);
    }

    /**
     * 根据键名查询参数配置信息
     * 
     * @param configKey 参数key
     * @return 参数键值
     */
    @Override
    public String selectConfigByKey(String configKey)
    {
        FlyConfig config = new FlyConfig();
        config.setConfigKey(configKey);
        FlyConfig retConfig = configMapper.selectConfig(config);
        return StrUtils.isNotNull(retConfig) ? retConfig.getConfigValue() : "";
    }

    /**
     * 查询参数配置列表
     * 
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    @Override
    public List<FlyConfig> selectConfigList(FlyConfig config)
    {
        return configMapper.selectConfigList(config);
    }

    /**
     * 新增参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public int insertConfig(FlyConfig config)
    {
        return configMapper.insertConfig(config);
    }

    /**
     * 修改参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public int updateConfig(FlyConfig config)
    {
        return configMapper.updateConfig(config);
    }

    /**
     * 删除参数配置信息
     * 
     * @param configId 参数ID
     * @return 结果
     */
    @Override
    public int deleteConfigById(Long configId)
    {
        return configMapper.deleteConfigById(configId);
    }

    /**
     * 批量删除参数信息
     * 
     * @param configIds 需要删除的参数ID
     * @return 结果
     */
    @Override
    public int deleteConfigByIds(Long[] configIds)
    {
        return configMapper.deleteConfigByIds(configIds);
    }

    /**
     * 校验参数键名是否唯一
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public String checkConfigKeyUnique(FlyConfig config)
    {
        Long configId = StrUtils.isNull(config.getConfigId()) ? -1L : config.getConfigId();
        FlyConfig info = configMapper.checkConfigKeyUnique(config.getConfigKey());
        if (StrUtils.isNotNull(info) && info.getConfigId().longValue() != configId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
}
