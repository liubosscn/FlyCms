package com.flycms.common.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class EmotionsUtils {

    private static String parse(String content){
        Map<String, String> kvs = new HashMap<>();
        kvs.put("[呵呵]", "<img src='/images/facebox/1.gif'>");
        kvs.put("[嘻嘻]", "<img src='/images/facebox/2.gif'>");
        kvs.put("[哈哈]", "<img src='/images/facebox/3.gif'>");
        kvs.put("[可爱]", "<img src='/images/facebox/4.gif'>");
        kvs.put("[可怜]", "<img src='/images/facebox/5.gif'>");
        kvs.put("[挖鼻屎]", "<img src='/images/facebox/6.gif'>");
        kvs.put("[吃惊]", "<img src='/images/facebox/7.gif'>");
        kvs.put("[害羞]", "<img src='/images/facebox/8.gif'>");
        kvs.put("[挤眼]", "<img src='/images/facebox/9.gif'>");
        kvs.put("[闭嘴]", "<img src='/images/facebox/10.gif'>");
        kvs.put("[鄙视]", "<img src='/images/facebox/11.gif'>");
        kvs.put("[爱你]", "<img src='/images/facebox/12.gif'>");
        kvs.put("[泪]", "<img src='/images/facebox/13.gif'>");
        kvs.put("[偷笑]", "<img src='/images/facebox/14.gif'>");
        kvs.put("[亲亲]", "<img src='/images/facebox/15.gif'>");
        kvs.put("[生病]", "<img src='/images/facebox/16.gif'>");
        kvs.put("[太开心]", "<img src='/images/facebox/17.gif'>");
        kvs.put("[懒得理你]", "<img src='/images/facebox/18.gif'>");
        kvs.put("[右哼哼]", "<img src='/images/facebox/19.gif'>");
        kvs.put("[左哼哼]", "<img src='/images/facebox/20.gif'>");
        kvs.put("[嘘]", "<img src='/images/facebox/21.gif'>");
        kvs.put("[衰]", "<img src='/images/facebox/22.gif'>");
        kvs.put("[委屈]", "<img src='/images/facebox/23.gif'>");
        kvs.put("[吐]", "<img src='/images/facebox/24.gif'>");
        kvs.put("[打哈气]", "<img src='/images/facebox/25.gif'>");
        kvs.put("[抱抱]", "<img src='/images/facebox/26.gif'>");
        kvs.put("[怒]", "<img src='/images/facebox/27.gif'>");
        kvs.put("[疑问]", "<img src='/images/facebox/28.gif'>");
        kvs.put("[馋嘴]", "<img src='/images/facebox/29.gif'>");
        kvs.put("[拜拜]", "<img src='/images/facebox/30.gif'>");
        kvs.put("[思考]", "<img src='/images/facebox/31.gif'>");
        kvs.put("[汗]", "<img src='/images/facebox/32.gif'>");
        kvs.put("[困]", "<img src='/images/facebox/33.gif'>");
        kvs.put("[睡觉]", "<img src='/images/facebox/34.gif'>");
        kvs.put("[钱]", "<img src='/images/facebox/35.gif'>");
        kvs.put("[失望]", "<img src='/images/facebox/36.gif'>");
        kvs.put("[酷]", "<img src='/images/facebox/37.gif'>");
        kvs.put("[花心]", "<img src='/images/facebox/38.gif'>");
        kvs.put("[哼]", "<img src='/images/facebox/39.gif'>");
        kvs.put("[鼓掌]", "<img src='/images/facebox/40.gif'>");
        kvs.put("[晕]", "<img src='/images/facebox/41.gif'>");
        kvs.put("[悲伤]", "<img src='/images/facebox/42.gif'>");
        kvs.put("[抓狂]", "<img src='/images/facebox/43.gif'>");
        kvs.put("[黑线]", "<img src='/images/facebox/44.gif'>");
        kvs.put("[阴险]", "<img src='/images/facebox/45.gif'>");
        kvs.put("[怒骂]", "<img src='/images/facebox/46.gif'>");
        kvs.put("[心]", "<img src='/images/facebox/47.gif'>");
        kvs.put("[伤心]", "<img src='/images/facebox/48.gif'>");
        kvs.put("[猪头]", "<img src='/images/facebox/49.gif'>");
        kvs.put("[ok]", "<img src='/images/facebox/50.gif'>");
        kvs.put("[耶]", "<img src='/images/facebox/51.gif'>");
        kvs.put("[good]", "<img src='/images/facebox/52.gif'>");
        kvs.put("[不要]", "<img src='/images/facebox/53.gif'>");
        kvs.put("[赞]", "<img src='/images/facebox/54.gif'>");
        kvs.put("[来]", "<img src='/images/facebox/55.gif'>");
        kvs.put("[弱]", "<img src='/images/facebox/56.gif'>");
        kvs.put("[蜡烛]", "<img src='/images/facebox/57.gif'>");
        kvs.put("[钟]", "<img src='/images/facebox/58.gif'>");
        kvs.put("[蛋糕]", "<img src='/images/facebox/59.gif'>");
        kvs.put("[话筒]", "<img src='/images/facebox/60.gif'>");
        kvs.put("[围脖]", "<img src='/images/facebox/61.gif'>");
        kvs.put("[转发]", "<img src='/images/facebox/62.gif'>");
        kvs.put("[路过这儿]", "<img src='/images/facebox/63.gif'>");
        kvs.put("[变脸]", "<img src='/images/facebox/64.gif'>");
        kvs.put("[困]", "<img src='/images/facebox/65.gif'>");
        kvs.put("[生闷气]", "<img src='/images/facebox/66.gif'>");
        kvs.put("[不要啊]", "<img src='/images/facebox/67.gif'>");
        kvs.put("[泪奔]", "<img src='/images/facebox/68.gif'>");
        kvs.put("[运气中]", "<img src='/images/facebox/69.gif'>");
        kvs.put("[有钱]", "<img src='/images/facebox/70.gif'>");

        Pattern p = Pattern.compile("(\\[)([[\\u4E00-\\u9FA5]\\w]+)(\\])");
        Matcher m = p.matcher(content);
        StringBuffer sr = new StringBuffer();
        while(m.find()){
            String group = m.group();
            m.appendReplacement(sr, kvs.get(group));
        }
        m.appendTail(sr);
        return sr.toString();
    }

    public static void main(String[] args) {

        System.out.println(parse("例如有这样一个 [变脸] 字符串[运气中]字符串：用户'${a}'的名称${b}"));
    }
 
}
